// -*- tab-width: 2; -*-
package it.lundstedt.erik.compendiumNovice;

import it.lundstedt.erik.seventhSon.api.languages.ScriptLanguage;
import it.lundstedt.erik.seventhSon.api.languages.TypeNames;
import it.lundstedt.erik.seventhSon.util.InitSpells;
import it.lundstedt.erik.seventhSon.util.PlayerScriptContext;
import it.lundstedt.erik.seventhSon.util.documentation.Documentation;
import it.lundstedt.erik.seventhSon.util.documentation.FunctionDocumentation;
import it.lundstedt.erik.seventhSon.util.function.ScriptFunction1;
import it.lundstedt.erik.seventhSon.util.packageLocation.PackageLocation;
import net.minecraft.text.LiteralText;
import org.jetbrains.annotations.Nullable;
import it.lundstedt.erik.seventhSon.util.function.ScriptFunction2;
import it.lundstedt.erik.seventhSon.util.function.ScriptFunction3;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.projectile.ArrowEntity;
import net.minecraft.entity.projectile.FireballEntity;
import net.minecraft.entity.projectile.PersistentProjectileEntity;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.Vec3d;




public class NoviceCompendiumSpellInit implements InitSpells {


	@Override
	public void initSpells(RegistrationFunction registry, PlayerScriptContext<?> context, ScriptLanguage<?> language) {
		PackageLocation novice = registry.addPackage("novice", new Documentation("Functions for the Novice."));

		//Example function.
		ScriptFunction1 test = new ScriptFunction1<String, Number>() {
			@Nullable
			@Override
			public FunctionDocumentation getDocumentation() {
				return new FunctionDocumentation("Simple testing function.")
						.addArgument("input", TypeNames.INTEGER, "Testing number.")
						.setReturnInfo(TypeNames.STRING, "The input as a string.");
			}

			@Override
			public String execute(Number input) {
				context.getCaster().getPlayer().ifPresent(player -> {
					player.sendMessage(new LiteralText("Testing " + input), false);
				});
				return input + "";
			}
		};
		ScriptFunction2 fireball=new ScriptFunction2<Void, Number, Vec3d>() {

			@Override
			public FunctionDocumentation getDocumentation() {
				return new FunctionDocumentation("creates a fireball.")
								.addArgument("power", TypeNames.DOUBLE, "explosionPower and time-on-fire")
								.addArgument("location", TypeNames.VEC3D, "where should the fireball be spawned");
			}



			@Override
			public Void execute() {
				return execute(10);
			}

			@Override
			public Void execute(Number number) {
				return execute(number, context.getCaster().getPos());
			}

			@Override
			public Void execute(Number number, Vec3d pos) {
				int pow = number.intValue();
				if (!context.drainManaIfPossible(pos.distanceTo(context.getCaster().getPos()) + pow * 100, false)) return null;
				FireballEntity entity = EntityType.FIREBALL.create(context.getCaster().getWorld());
				entity.explosionPower=pow;
				entity.setOnFireFor(pow);
				
				entity.refreshPositionAndAngles(pos.getX(), pos.getY(), pos.getZ(), 0, 0);
				context.getCaster().getWorld().spawnEntity(entity);
				return null;
			}
		};
		
		
		ScriptFunction3 magicArrow= new ScriptFunction3<Void, Number, Number, Boolean>() {
			@Override
			public FunctionDocumentation getDocumentation() {
				return new FunctionDocumentation("Shoots an arrow.")
						.addArgument("force", TypeNames.DOUBLE, "The force of the arrow, any value between 0 and 64.")
						.addArgument("power", TypeNames.DOUBLE, "The power of the arrow, defaults to 2.")
						.addArgument("fire", TypeNames.BOOLEAN, "If true, it will be on fire.");
			}
			
			@Override
			public Void execute() {
				return execute(2);
			}
			
			@Override
			public Void execute(Number force) {
				return execute(force,2);
			}
			
			@Override
			public Void execute(Number force, Number power) {
				return execute(force, power,false);
			}
			
			
			@Override
			public Void execute(Number force, Number power, Boolean fire) {
				//FireballEntity entity = EntityType.FIREBALL.create(context.getCaster().getWorld());
				
				context.getCaster().getLivingEntity().ifPresent(caster -> {
					ArrowEntity entity = new ArrowEntity(context.getCaster().getWorld(), caster);
					entity.pickupType = PersistentProjectileEntity.PickupPermission.CREATIVE_ONLY;
					
					double manaDrain = 100;
					
					float actualForce = MathHelper.clamp((float) force.doubleValue() / 64, 0, 1);
					
					manaDrain += actualForce * 100;
					
					entity.setProperties(caster, context.getCaster().getPitch(), context.getCaster().getYaw(), 0.0f, actualForce * 3.0f, 1.0f);
					
					double pow = power.doubleValue();
					entity.setDamage(pow);
					
					manaDrain += (pow - 2) * 100;
					
					if (actualForce == 1) {
						entity.setCritical(true);
					}
					
					if (fire) {
						entity.setOnFireFor(100);
						manaDrain += 50;
					}
					
					if (!context.drainManaIfPossible(manaDrain, false)) return;
					
					context.getCaster().getWorld().spawnEntity(entity);
				});
				return null;
			}
		};
		
		
		
		registry.register(novice,"test",test);
		registry.register(novice,"fireball",fireball);
		registry.register(novice,"magicArrow",magicArrow);
		
	}
}

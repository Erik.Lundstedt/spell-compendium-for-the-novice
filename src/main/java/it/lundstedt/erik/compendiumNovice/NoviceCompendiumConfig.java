package it.lundstedt.erik.compendiumNovice;

import me.lortseam.completeconfig.api.ConfigContainer;
import me.lortseam.completeconfig.api.ConfigEntries;
import me.lortseam.completeconfig.api.ConfigGroup;
import me.lortseam.completeconfig.data.Config;

@ConfigEntries
public class NoviceCompendiumConfig extends Config implements ConfigContainer {

	public NoviceCompendiumConfig() {
		super(NoviceCompendiumMain.MODID);
	}




	@Transitive
	@ConfigEntries
	public static class Client implements ConfigGroup {


	}

}

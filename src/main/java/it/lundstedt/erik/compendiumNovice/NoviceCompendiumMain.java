package it.lundstedt.erik.compendiumNovice;

import net.fabricmc.api.ModInitializer;
import net.fabricmc.loader.api.FabricLoader;
import net.minecraft.text.TranslatableText;
import net.minecraft.util.Identifier;

import java.nio.file.Path;


public class NoviceCompendiumMain implements ModInitializer {

	public static final String MODID = "seventh_son_novice_spell_compendium";

	protected static NoviceCompendiumConfig config;

	public static Path configFolder = FabricLoader.getInstance().getConfigDir().resolve(MODID);




	@Override
	public void onInitialize() {
		config = new NoviceCompendiumConfig();
		config.load();


	}

	public static Identifier getId(String input) {
		return new Identifier(MODID, input);
	}

	public static String getTranslationString(String type, String object, String text) {
		String dot = ".";
		return type + dot + MODID + dot + object + dot + text;
	}

	public static TranslatableText getTranslatableText(String type, String object, String text, Object... args) {
		return new TranslatableText(getTranslationString(type, object, text), args);
	}

	public static String getTranslationString(String type, String text) {
		String dot = ".";
		return type + dot + MODID + dot + text;
	}

	public static TranslatableText getTranslatableText(String type, String text, Object... args) {
		return new TranslatableText(getTranslationString(type, text), args);
	}

}

package it.lundstedt.erik.compendiumNovice.client;

import it.lundstedt.erik.compendiumNovice.NoviceCompendiumMain;
import me.lortseam.completeconfig.gui.ConfigScreenBuilder;
import me.lortseam.completeconfig.gui.cloth.ClothConfigScreenBuilder;
import net.fabricmc.api.ClientModInitializer;

public class MainClient implements ClientModInitializer {

	@Override
	public void onInitializeClient() {
		ConfigScreenBuilder.setMain(NoviceCompendiumMain.MODID, new ClothConfigScreenBuilder());
	}
}
